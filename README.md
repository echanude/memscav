# Deprecation

The main project is now hosted in the CentOS Automotive SIG:
* https://gitlab.com/CentOS/automotive/src/kmod-memscav
* https://gitlab.com/CentOS/automotive/rpms/kmod-memscav

# memscav

`memscav` is an out-of-tree Linux kernel module that allows userspace to
reclaim memory potentially excluded by the kernel parameter `mem=X`. It
currently supports kernel using device-tree (`/memory` node) or UEFI memory
map.

## Build

```
make [KERNEL_SRC=<path-to-kernel-tree>]
```

## Example

```
# cat /proc/cmdline
[...]mem=4g [...]
# free -g
               total        used        free      shared  buff/cache   available
Mem:               3           0           2           0           0           2
Swap:              0           0           0
# insmod memscav.ko
# echo $((34<<30)) > /sys/kernel/memscav/scavenge
# free -g
               total        used        free      shared  buff/cache   available
Mem:              34           1          33           0           0          32
Swap:              0           0           0
# grep memscav /proc/iomem
000000000-000000000 : System RAM (memscav)
000000000-000000000 : System RAM (memscav)
000000000-000000000 : System RAM (memscav)
000000000-000000000 : System RAM (memscav)
```

## Sysfs interface

```
What:		/sys/kernel/memscav
Date:		October 2023
KernelVersion:	downstream
Description:
		Interface to control memory scavenging options. Memory past the
		limited amount instructed to the kernel can be recovered using
		the platform description.
		This is similar to what CONFIG_ARCH_MEMORY_PROBE allows, with
		some additional simple checks.
		(see Documentation/admin-guide/mm/memory-hotplug.rst)

What:		/sys/kernel/memscav/probe
Date:		October 2023
KernelVersion:	downstream
Description:
		Write-only. Similar to /sys/devices/system/memory/probe.
		Does not perform any sanity check, it is therefore possible to
		create a virtual mapping to memory that does not exist. For
		debug and experimentation only.
		(see Documentation/admin-guide/mm/memory-hotplug.rst)

What:		/sys/kernel/memscav/ranges
Date:		October 2023
KernelVersion:	downstream
Description:
		Read-only. Reports the known physical ranges for RAM as reported by the
		platform (device-tree or UEFI memory map) to the kernel.

What:		/sys/kernel/memscav/scavenge
Date:		October 2023
KernelVersion:	downstream
Description:
		Write-only. Takes an amount of bytes and tries to recover
		physical memory reported during boot but excluded by the kernel
		(for example, by the parameter "mem=X").
		Fails with EINVAL if the amount given is not a multiple of
		/sys/devices/system/memory/block_size_bytes.
		This will scavenge as much memory as possible, but there is no
		guaranty that the total amount will be scavenged.
		/sys/kernel/memscav/hidden_blocks will show if any memory block is
		still hidden.

What:		/sys/kernel/memscav/hidden_blocks
Date:		October 2023
KernelVersion:	downstream
Description:
		Read-only. Reports blocks of physical addresses currently hidden from the system.
		When using /sys/kernel/memscav/scavenge, the recovered memory will be
		taken per block-size from these ranges.
```
