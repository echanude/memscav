#! /usr/bin/bash
# SPDX-License-Identifier: LGPL-2.1+

# Prerequisite check(s) for module.
check() {
	# Return 255 to only include the module, if another module requires it.
	return 255
}

# Module dependency requirements.
depends() {
	# Return 0 to include the dependent module(s) in the initramfs.
	return 0
}

# Install the required file(s) and directories for the module in the initramfs.
install() {
	# Install initramfs required files.
	inst_multiple -o \
		"$systemdsystemunitdir"/memscav-initrd.service \
		/usr/libexec/memscav/memscav-initrd \
		/etc/memscav.conf

	"$SYSTEMCTL" -q --root "$initdir" enable memscav-initrd.service
}
